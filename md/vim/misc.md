gq						Reformat auto format using colorcolumn
\n%s//replacement/		Word replace - find empty matches last search
\pl						Load project session
\ps						Save project session
:call ProjectSet		Set .vimproject folder
Ctrl+]					Tag: Go to the function definition
Ctrl+T					Tag: Go back from the function definition to start point
