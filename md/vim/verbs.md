# verbs
`v` visual  `c` change    `d` delete        `y` yank/copy

# objects
`w` word             `s`   sentence           `p` paragraph  
`b` block            `t`   tag(html/xml)
`G` last line        `10G` to the 10th line
`0` line start       `$`   line end

# modifiers
`a` around  `i` inside    `t` till a char   `f` find
`/` down search regex     `?` up search regex

# examples

`diw`   delete current word
`di{`   delete inside `{ }`
`d/x`   delete until next ocurrence of `x`
`dap`   delete current paragraph
`vap`   select current paragraph

