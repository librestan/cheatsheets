\f                  open file navigation
A                   window zoom toggle
B                   show bookmarks
cd                  change current dir (CWD) to selected dir
CD                  change tree root to current dir (CWD)
C                   change tree root to selected directory
D                   delete bookmark
f                   toggle file filters
F                   toggle files
gi                  preview split
gs                  preview vsplit (vertical)
i                   open split
I                   toggle show hidden files
m                   show menu
o                   open bookmark
q                   close window
s                   open vsplit (vertical)
U                   move tree root up directory
:EditBookmarks      edit bookmarks
:WriteBookmarks     write bookmarks
