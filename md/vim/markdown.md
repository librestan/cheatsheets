`ge`     Open in vim
`gx`     Open on xdg-open browser

`:Toc`   Toc in vertical quickfix
`:Toch`  Toc on horizontal quickfix
`:Toct`  Open toc in new tab

`]]`     go to next header.
`[[`     go to previous header. Contrast with ]c.
`][`     go to next sibling header if any.
`[]`     go to previous sibling header if any.
`:1]c`   go to Current header.
`]u`     go to parent header (Up).
