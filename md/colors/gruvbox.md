# Gruvbox colorscheme

 Name    | D # | Dark    | N # | Normal  | L # | Light
 :-------|----:|:-------:|----:|:-------:|----:|:-------:
 black   | 234 | #1d2021 | 237 | #3c3836 | 241 | #665c54
 red     |  88 | #9d0006 | 124 | #cc241d | 167 | #fb4934
 green   | 100 | #79740e | 106 | #98971a | 142 | #b8bb26
 yellow  | 136 | #b57614 | 172 | #d79921 | 214 | #fabd2f
 blue    |  24 | #076678 |  66 | #458588 | 109 | #83a598
 magenta |  96 | #8f3f71 | 132 | #b16286 | 175 | #d3869b
 aqua    |  66 | #427b58 | 72  | #689d6a | 108 | #8ec07c
 white   | 250 | #d5c4a1 | 223 | #ebdbb2 | 229 | #fbf1c7
 orange  | 166 | #af3a03 | 166 | #d65d0e | 208 | #fe8019
 grey    | 243 | #7c6f64 | 245 | #928374 | 246 | #a89984
 
## Neutral
 
 Name    | D # | Dark    | N # | Normal  | L # | Light
 :-------|----:|:-------:|----:|:-------:|----:|:-------:
 black   | 234 | #1d2021 | 237 | #3c3836 | 241 | #665c54
 grey    | 243 | #7c6f64 | 245 | #928374 | 246 | #a89984
 white   | 250 | #d5c4a1 | 223 | #ebdbb2 | 229 | #fbf1c7
 

## Reference images:

![lighter](https://camo.githubusercontent.com/410b3ab80570bcd5b470a08d84f93caa5b4962ccd994ebceeb3d1f78364c2120/687474703a2f2f692e696d6775722e636f6d2f776136363678672e706e67)
![darker](https://camo.githubusercontent.com/d080d9c204408ef06b862b76bc795f930b3a9b1be4c5d2de149f1d8eb765b660/687474703a2f2f692e696d6775722e636f6d2f3439714b7959572e706e67)
