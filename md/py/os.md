# Path

Resolve path          `os.path.realpath( path )`
Dir from a file       `os.path.dirname( file )`


# File Handling

Create dir recursive:  `os.makedirs( path, 0o755, ok_exists=True )`
Read Contents:         `while open( file ) as f: content=f.readlines() `
